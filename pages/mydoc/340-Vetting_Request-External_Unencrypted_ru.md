---
title: Vetting Request - External Unencrypted - Russian
keywords: email templates, vetting, vetting process, partner
last_updated: February 7, 2019
tags: [helpline_procedures_templates, templates]
summary: "Внешняя проверка, письмо партнеру (не шифрованное)"
sidebar: mydoc_sidebar
permalink: 340-Vetting_Request-External_Unencrypted_ru.html
folder: mydoc
conf: Public
ref: Vetting_Request-External_Unencrypted
lang: ru
---


# Vetting Request - External Unencrypted
## External Vetting, Email to Contact if unencrypted

### Body

Здравствуйте,

Меня зовут [ИМЯ СОТРУДНИКА]. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now. Ваши контактные данные были получены мной от [ИМЯ СОТРУДНИКА ACCESS NOW ИЛИ ПАРТНЕРА, НО НЕ ИМЯ КЛИЕНТА]. Я обращаюсь к вам с просьбой относительно нового клиента нашей службы поддержки.

В соответствии с принятыми у нас правилами мы связываемся с доверенными контактами, чтобы проверить личность нового клиента. Чтобы продолжить разговор, нужно установить безопасный канал связи. 

Мы можем помочь вам с настройкой PGP для шифрования электронной почты или мессенджера Signal. Пожалуйста, сообщите, если вам нужна такая помощь. 

С уважением,

[ИМЯ СОТРУДНИКА]
