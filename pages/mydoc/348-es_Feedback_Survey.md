---
title: Spanish - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 05, 2019
tags: [helpline_procedures_templates, templates]
summary: "Template containing feedback survey to send to Spanish-speaking clients when closing a ticket"
sidebar: mydoc_sidebar
permalink: 348-es_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: es
---


# Feedback Survey Template - Spanish
## Spanish email template containing feedback survey to send to clients when closing a ticket

### Body

Hola $ClientName,

Gracias por contactar a la Línea de Ayuda en Seguridad Digital, dirigida por la organización internacional de derechos humanos Access Now - https://accessnow.org.

Este mensaje es para notificarle del cierre de su caso titulado "{$Ticket-&gt;Subject}".

Su opinión es importante para nosotros. Si desea proporcionar comentarios acerca de su experiencia con la Línea de Ayuda de Access Now, por favor complete el siguiente formulario:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Su número de caso es: {$Ticket-&gt;id}

Si tiene preguntas adicionales, por favor infórmenos y estaremos felices de ayudarle.

Gracias,

$IHName
