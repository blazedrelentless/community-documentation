---
title: File Encryption with VeraCrypt or GPG
keywords: file encryption, Veracrypt, GPG, Linux, backup
last_updated: August 27, 2019
tags: [devices_data_security, articles]
summary: "A client is uploading sensitive data on an online file hosting service (DropBox-like) without protecting it through encryption, or needs to back up sensitive data securely in storage devices. They are looking for an open source, trustworthy and free tool to encrypt files."
sidebar: mydoc_sidebar
permalink: 210-File_Encryption_VeraCrypt_GPG.html
folder: mydoc
conf: Public
ref: File_Encryption_VeraCrypt_GPG
lang: en
---


# File Encryption with Veracrypt or GPG
## How to encrypt files with free and open source tools

### Problem

- Sensitive information is being sent or stored online with no protection or encryption.
- Sensitive files need to be protected in case a computer or storage device is stolen or lost or there is a break-in in the client's office.
- Backups are currently stored with no protection or encryption.
- Need to send sensitive data to a specific person encrypting the data with their public key and uploading it to a hosting service.


* * *


### Solution

#### VeraCrypt

[VeraCrypt](https://www.veracrypt.fr) is a multi-platform free and open source tool that helps encrypt files or entire storage devices. It can also be used to store sensitive files in a hidden volume, that cannot be found even if the standard encrypted volume is accessed.

It is the recommended tool for encrypting files or external storage devices.

- [VeraCrypt Download Page](https://www.veracrypt.fr/en/Downloads.html)
- [VeraCrypt Beginner’s Tutorial](https://www.veracrypt.fr/en/Beginner%27s%20Tutorial.html)
- [VeraCrypt Official Documentation](https://www.veracrypt.fr/en/Documentation.html)

*If the client is a high-risk user, we should strongly recommend they verify the installer against its PGP signature (available in the downloads page next to each installer) before launching the installation, guiding them step by step with instructions on how to do it.*


#### Encrypt Files with GPG

If the client already uses GPG and needs to encrypt single files for their personal usage or to share them with other people who use GPG, GPG-encryption can also be used.

##### Linux

Users who already have a PGP key can encrypt and decrypt files using GPG.

- Command line:

    - To encrypt:

            gpg -c filename
        
    - To decrypt:

            gpg filename.gpg
        
- GUI:

    - Gnome - Seahorse and Nautilus:
        - [Tutorial](http://blog.programster.org/nautilus-gpg-integration)
    -  KDE - KGpg and Konqueror or Dolphin_
        - [Official KGpg documentation](https://docs.kde.org/trunk5/en/kdeutils/kgpg/kgpg.pdf#subsection.3.3.1)

##### macOS

For Mac users, please refer to [Article #76: Encrypt files on a Mac with GpgTools](76-Encrypt_files_Mac_GpgTools.html) for encrypting files using GPG Tools.

##### Windows

If the client has already installed Gpg4win, they can use GpgEX and Kleopatra to encrypt and decrypt files.

- [Official Gpg4win tutorial](https://www.gpg4win.org/doc/en/gpg4win-compendium_24.html#id5)

Windows users who only need to share encrypted files with other Windows users, can use the in-built Encrypting File System (EFS) feature to encrypt their files.

- [Official guide](https://support.microsoft.com/en-us/help/4026312/windows-10-how-to-encrypt-a-file)


* * *


### Comments

More alternatives for file encryption:

- [AesCrypt](https://www.aescrypt.com/) - AES Crypt is free and open source file encryption software available on several operating systems that uses the industry standard Advanced Encryption Standard (AES) to easily and securely encrypt files
- [Safe](http://www.getsafe.org/about) - Safe is a free and open software application to encrypt files. It runs on Windows and macOS.

If the client needs to encrypt the hard drive of their computer, see [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html).

If the client needs to encrypt an external storage device, see [Article #214: Encrypt an External Storage Device with VeraCrypt](214-Encrypt_External_Storage_Device_VeraCrypt.html).

For secure online storage, also see [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html).


* * *

### Related Articles

- [Article #76: Encrypt files on a Mac with GpgTools](76-Encrypt_files_Mac_GpgTools.html)
- [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)
- [Article #214: Encrypt an External Storage Device with VeraCrypt](214-Encrypt_External_Storage_Device_VeraCrypt.html)
- [Article #282: Recommendations on Secure File Sharing and File Storage](282-Secure_file_sharing_storage.html)
