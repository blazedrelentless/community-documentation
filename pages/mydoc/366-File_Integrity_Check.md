---
title: How to Check the Integrity of a File
keywords: file integrity, file verification, hash, integrity check, GnuPG, GPG, SHA256, digital signature
last_updated: January 8, 2020
tags: [devices_data_security, articles]
summary: "A client needs to check the integrity of a file they have downloaded from the internet, or to extract the hash of a file that they are sending to the Helpline, so we can check the integrity of that file."
sidebar: mydoc_sidebar
permalink: 366-File_Integrity_Check.html
folder: mydoc
conf: Public
ref: File_Integrity_Check
lang: en
---


# How to Check the Integrity of a File
## Instructions to extract the hash of a file and/or check its integrity

### Problem

When installing software or sending files to the Helpline or other experts for forensic analysis, clients need to be aware that the files they have downloaded or sent can be corrupted. The reasons why a file can be corrupted are many: file integrity can be compromised due to Man-in-the-middle attacks, because the website where the file was stored has been compromised, or due to faulty storage media, errors in transmission, write errors during copying or moving, software bugs, etc.


* * *


### Solution

Hash-based verification ensures that a file has not been corrupted by comparing the hash value of the file to a previously calculated value. If these values match, the file is presumed to be unmodified.

#### Generate the Hash of a File

Generating the hash of a file can be required in several scenarios:

1. If a client is sending a file for analysis, it is required to document the hash of the first copy of the files received by the client. Therefore, we first need to guide the client through the process of generating the hash and ask them to communicate it to us through a secure and verified channel.

    When we receive the file, we will then need to generate the hash of the received copy and compare it to the one the client sent. In this way, we can verify the integrity of the file to make sure that what we received is identical to what the client sent.

2. If a client needs to verify the integrity of a file they have downloaded from the internet, and the only way to do so is compare it against its hash.

##### Instructions for Linux or macOS

Open a terminal and enter the command below for each file:

        shasum -a 256 [path to the file]

This will print the sha256 hash of the file in the terminal.


##### Instructions for Windows

- **Windows Certutil**
    - Open a new CMD window from the Start Menu.
    - Navigate to the directory where your file is.
    - Enter the following command:

            certutil -hashfile your-file SHA256

    - This will print the file’s checksum in the console.
    - If you want to verify the checksum against a different algorithm such as SHA512 or MD5, replace SHA256 in your command with the desired algorithm.

    A tutorial on hash generation and file verification with Windows Certutil can be found [here](https://www.thewindowsclub.com/verify-md5-checksum-of-files-using-certutil).
    
- **Windows PowerShell**
    - Launch Windows PowerShell (installed by default in every Windows, starting with Windows 7 SP1 and Windows Server 2008 R2 SP1) and then launch this command:

            Get-FileHash $path\to-file | Format-List

        More information on the `Get-FileHash` command can be found [in the official PowerShell documentation](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-6).


#### GnuPG - Integrity and Authenticity Verification

Whenever possible, files that have been downloaded from the internet should be checked against a GPG signature, after having installed and verified the GPG key the signature was generated with. This will help verify both the integrity and the authenticity of the file, i.e. to make sure that the file and its hash haven't been tampered with by malicious actors after they were published by their creators.

To verify a file against its GnuPG signature, guide the client through the following steps based on their operating system, especially if they are planning to use that file to install sensitive software in their computer or for forensic purposes.

##### Windows

- If the client isn't using GPG in their computer yet, send them [these instructions on how to install Gpg4win](https://guides.accessnow.org/PGP_Encrypted_Email_Windows.html#download-and-install-gpg4win).

    *If the client doesn't need to encrypt their email, please make sure that they only follow the instructions in the section on how to download and install Gpg4win.*

- Import the GPG key that was used to generate the file signature following [these instructions](https://www.gpg4win.org/doc/en/gpg4win-compendium_15.html).

- Verify the GPG key against its fingerprint, following the instructions in the "Fingerprints" section of [this tutorial](https://www.gpg4win.org/doc/en/gpg4win-compendium_16.html):
    - check that the fingerprint you see in the details of the imported certificate matches with the one provided by the owner of the GPG key (this can usually be found in the same place where you downloaded the GPG key).
    - If the fingerprints match, you can proceed with the verification of the file based on its GPG signature.

- Configure the trust level for the GPG key you have imported and verified:
    - Select the certificate
    - From the main menu select Certificates -&gt; Change Owner Trust
    - Select "I believe checks are very accurate" and press OK.

- Download the GPG signature in the same folder where you have downloaded the file. This will be a file ending with .sig or .asc that you usually find in the same place where you downloaded the file that you need to verify.

-  To verify the file against the signature, either drag and drop the signature file into Kleopatra, or load the dialog from File -&gt; Decrypt/Verify Files, and then choose either the file you want to verify or its signature.

- Click Decrypt/Verify to check the file. The result may contain a yellow warning, which looks problematic but means that the file verification was successful.

    **If, on the other hand, you see a red "The signature is bad" error, this means the integrity of your file is compromised.**


##### macOS

- If the client isn't using GPG in their computer yet, send them [these instructions on how to install GPG Suite](https://guides.accessnow.org/PGP_Encrypted_Email_Mac.html#download-and-install-gpg-suite).

    *If the client doesn't need to encrypt their email, please make sure that they only follow the instructions in the section on how to download and install GPG Suite.*

- Import the GPG key that was used to generate the file signature:
    - If the key is an .asc file, drag that file into GPG Keychain Access.
    - If the key is in text form, copy it and hit paste when in GPG Keychain.

- Verify the GPG key against its fingerprint:
    - Open the GPG Keychain window and locate the key you have just imported. Check that its fingerprint matches with the one provided by the owner of the GPG key (this can usually be found in the same place where you downloaded the GPG key).
    - If the fingerprints match, you can proceed with the verification of the file based on its GPG signature.

- Download the GPG signature in the same folder where you have downloaded the file. This will be a file ending with .sig or .asc that you usually find in the same place where you downloaded the file that you need to verify.

- Right-click the signature or file and select Services -&gt; OpenPGP: Verify Signature of File.

- If everything is ok, the verification result will confirm that the file has been signed with the GPG key you have imported.


##### Linux

- Import the GPG key that was used to generate the file signature, either by command line, or with tools like Enigmail or KGpg.
    - [command line](https://www.gnupg.org/gph/en/manual/x56.html#AEN83)
    - [Enigmail](https://www.enigmail.net/index.php/en/user-manual/key-management#Importing_public_keys)
    - On KGpg, click Keys -&gt; Import Key, then select the file of the GPG key you want to import and click OK.

- Verify the GPG key against its fingerprint:
    - in a terminal, launch the following commands:
         
            gpg --list-keys

        all present keys will be displayed. Identify the ID of the key you just imported.

            gpg --fingerprint ID-of-your-key

        the fingerprint of the imported key will be printed in your terminal. Check it against the one provided by the owner of the GPG key (this can usually be found in the same place where you downloaded the GPG key).

    - In Enigmail and KGpg, you will find the fingerprint in the key properties: right-click on the key in the key management window And then click "Key Properties".

- If the fingerprints match, you can proceed with the verification of the file based on its GPG signature.

- Download the GPG signature in the same folder where you have downloaded the file. This will be a file ending with .sig or .asc that you usually find in the same place where you downloaded the file that you need to verify.

- Verify the file against its signature by launching the following commands in a terminal:

        cd folder/containing/file/and/signature

        gpg --verify signature file 

    If the output contains the string "Good signature", you can consider your file verified.


* * *


### Comments


* * *

### Related Articles
