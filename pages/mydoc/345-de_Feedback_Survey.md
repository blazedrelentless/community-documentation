---
title: German - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "Email, die bei der Schlie­ßung eines Falles an all die KundInnen der Helpline geschickt wird"
sidebar: mydoc_sidebar
permalink: 345-de_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: de
---


# German - Feedback Survey Template
## Email, die bei der Schlie­ßung eines Falles an all die KundInnen der Helpline geschickt wird

### Body

Hallo $ClientName,

danke, daß du dich an die Digital Security Helpline von der internationalen Menschenrechtsorganisation Access Now - https://accessnow.org - gewendet hast.

Mit dieser Email möchten wir dich über die Schließung von deinem Fall, mit dem Titel "{$Ticket-&gt;Subject}", informieren.

Für uns ist dein Feedback wichtig. Wenn du deine Erfahrung mit Access Now Digital Security Helpline erzählen und kommentieren möchtest, fülle bitte diese Umfrage aus:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Deine Fallnummer ist:  {$Ticket-&gt;id}

Wenn du weitere Fragen oder Zweifel hast, wende dich bitte an uns: wir sind froh, dir zu helfen.

Danke schön,

$IHName
