---
title: Disable C&amp;C Server - Email to Hosting Provider - Russian
keywords: C&amp;C server, malware, email templates, hosting provider, malicious website
last_updated: February 6, 2019
tags: [vulnerabilities_malware_templates, templates]
summary: "Шаблон письма к хостинг-провайдеру с просьбой отключить вредоносный сайт"
sidebar: mydoc_sidebar
permalink: 312-Disable_Malicious_Server_hosting_provider_ru.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_hosting_provider
lang: ru
---


# Disable C&amp;C Server - Email to Hosting Provider
## Template for writing to the company that hosts a malicious website to disable it

### Body

Здравствуйте,

Меня зовут [ИМЯ СОТРУДНИКА]. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help). Нам сообщили, что веб-сайт на вашем хостинге заражает посетителей вредоносным кодом:

- [АДРЕС САЙТА] *[ЗАМЕНИТЕ HTTP НА HXXP, ЧТОБЫ ПРЕДОТВРАТИТЬ ЗАРАЖЕНИЕ]*
- [IP-АДРЕС]

В адресе (выше) мы заменили "http" на "hxxp", чтобы исключить возможность случайного заражения.

*[ЗАМЕНИТЕ СЛЕДУЮЩИЙ ПАРАГРАФ ВАШИМ АНАЛИЗОМ СИТУАЦИИ]*

На нескольких страницах обнаружен вредоносный скрипт JavaScript. Этот скрипт подгружает информацию с сайта третьего лица на компьютер посетителя. Этот сайт:

- [АДРЕС]

Пожалуйста, удалите вредоносный контент с вашего веб-сервера. Обновите все приложения (системы управления контентом, плагины и др.) и серверные компоненты (веб-сервер, FTP и прочие) до самых свежих версий и проверьте настройки безопасности всех компонентов.

Некоторые рекомендации вы можете найти здесь:

- https://www.circl.lu/pub/tr-26/

Пожалуйста, свяжитесь с нами, если у вас есть вопросы или вам нужна дополнительная поддержка.

Будем признательны, если вы подтвердите получение этого письма.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&amp;C Server](219-Targeted_Malware_Disable_Malicious_Server.html)
- [Article #259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)
